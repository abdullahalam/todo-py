# todo-py
An extremely simple, platform independent, command-line todolist. 

## Requirements
* Python 3.x

## Installation
Clone this repository to your computer, 

`git clone https://abdullahalam@bitbucket.org/abdullahalam/todo-py.git`

Or download the zip and extract it's files.

## Usage
* Windows:
Execute the todo.py file in *cmd*

`python todo.py`

* Linux/OS-X:
Execute the todo.py file in your terminal

`./todo.py`

## Info:
* Python 3.x needs to be installed for this program.
* Items are stored in `todolist.txt` in the same directory.
* Only single line items can be added.
