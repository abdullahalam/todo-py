#!/usr/bin/python3
import os


class Todo:

	todofiledir="./todolist.txt"

	def __init__(self):
		
		#OPEN FILE
		try:
			self.f=open(self.todofiledir, "r+")
		except:
			self.f=open(self.todofiledir, "w+")
		string = self.f.readline();
		self.items=string.split('|')
		if(self.items[0] == ''):
			self.items=[]
		self.f.close();		

	def listItems(self):
		os.system("clear")
		print("\n----------------------\n")
		for x in range(0,len(self.items)):
			print(x+1,": ", self.items[x])
		print("\n----------------------\n")

	def addItem(self, item):
		self.items.append(item)

	def removeItem(self, index):
		del self.items[index-1]

	def commitToFile(self):
		self.f=open(self.todofiledir, "w+")
		string=""
		for i in range(0,len(self.items)):
			string+=self.items[i]
			if(i<len(self.items)-1):
				string+="|"
		self.f.write(string)
		self.f.close()



t=Todo()
opt='i';


while(opt!='q'):
	t.listItems()
	print("\nAdd item\t[a]\nRemove item\t[r]\nQuit\t\t[q]\n\n")
	opt=input("Choose: ")
	if(opt=='a'):
		item=input("\n\nTask: ")
		t.addItem(item)
	elif (opt=='r'):
		n=int(input("\n\nItem number: "))
		t.removeItem(n)
		print("Good Job!")

t.commitToFile()
